package fr.the6thscreen.yourcast.customizer.service;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.google.code.morphia.Datastore;

import fr.the6thscreen.yourcast.customizer.authentication.SessionManager;
import fr.the6thscreen.yourcast.customizer.db.Utils;
import fr.the6thscreen.yourcast.customizer.schema.ProviderInfo;
import fr.the6thscreen.yourcast.customizer.schema.ServiceCall;
import fr.the6thscreen.yourcast.customizer.schema.ServiceInfo;

@Path("scall/")
public class ServiceCallService {
	private static Logger log = Logger.getLogger(ServiceCallService.class);
	
	@GET
	@Produces(Utils.SERVICE_PRODUCES)
	public Response getAllServiceCalls() {
		Datastore ds = Utils.getMongoConnection().getDatastore();
		List<ServiceCall> result = ds.find(ServiceCall.class).asList();
		return RestUtils.createResponse(result);
	}
	
	@GET
	@Path("{callID}")
	@Produces(Utils.SERVICE_PRODUCES)
	public Response getServiceCall(@PathParam("callID") String id) {
		Datastore ds = Utils.getMongoConnection().getDatastore();
		ServiceCall result = ds.find(ServiceCall.class, "callID =", id).get();
		return RestUtils.createResponse(result);
	}
	
	@GET
	@Path("source/{name}")
	@Produces(Utils.SERVICE_PRODUCES)
	public Response getServiceCallForServiceInfo(@PathParam("name") String name) {
		Datastore ds = Utils.getMongoConnection().getDatastore();
		ServiceInfo si = ds.find(ServiceInfo.class, "name = ", name).get();
		
		List<ServiceCall> result;
		if (ds.find(ServiceCall.class).countAll() > 0) {
			result = ds.find(ServiceCall.class, "service =", si).asList();
		} else {
			result = new ArrayList<ServiceCall>();
		}
		return RestUtils.createResponse(result);
	}
	
	@GET
	@Path("zone/{name}")
	@Produces(Utils.SERVICE_PRODUCES)
	public Response getServiceCallForProviderInfo(@PathParam("name") String name) {
		Datastore ds = Utils.getMongoConnection().getDatastore();
		ProviderInfo pi = ds.find(ProviderInfo.class, "name = ", name).get();
		List<ServiceCall> result;
		if (ds.find(ServiceCall.class).countAll() > 0) {
			result = ds.find(ServiceCall.class, "provider =", pi).asList();
		} else {
			result = new ArrayList<ServiceCall>();
		}
		return RestUtils.createResponse(result);
	}
	
	@GET
	@Path("source/{nameSource}/zone/{nameZone}")
	@Produces(Utils.SERVICE_PRODUCES)
	public Response getServiceCallForServiceInfoAndProviderInfo(@PathParam("nameSource") String nameSource, @PathParam("nameZone") String nameZone) {
		Datastore ds = Utils.getMongoConnection().getDatastore();
		ServiceInfo si = ds.find(ServiceInfo.class, "name = ", nameSource).get();
		ProviderInfo pi = ds.find(ProviderInfo.class, "name = ", nameZone).get();
		// TODO Check if the table exists
		
		List<ServiceCall> result;
		
		if (ds.find(ServiceCall.class).countAll() > 0) {
			result = ds.find(ServiceCall.class, "service =", si).filter("provider = ", pi).asList();
		} else {
			result = new ArrayList<ServiceCall>();
		}
		return RestUtils.createResponse(result);
	}
}
