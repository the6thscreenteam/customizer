package fr.the6thscreen.yourcast.customizer.service;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import com.google.code.morphia.Datastore;

import fr.the6thscreen.yourcast.customizer.db.Utils;
import fr.the6thscreen.yourcast.customizer.schema.*;

import org.apache.log4j.Logger;


@Path("/")
public class CrudService {

	final Logger log = Logger.getLogger(CrudService.class.getName());
	
	@GET
	@Produces(Utils.SERVICE_PRODUCES)
	public Response checkDatabase() {
		DatabaseFlag df = null;
		Datastore ds = null;
		if (!Utils.getMongoConnection().isDatabaseExisting()) {
			log.debug("The database doesn't exist => creation of DB !");
			Utils.getMongoConnection().createDatabase();
			ds = Utils.getMongoConnection().getDatastore();
		} else {
			ds = Utils.getMongoConnection().getDatastore();
			df = ds.find(DatabaseFlag.class).get();
			if (df == null || !df.isBuilt()) {
				log.debug("Database flag cannot be retrieve or built flag is false => creation of DB !");
				Utils.getMongoConnection().createDatabase();
			}
		}
		if (df == null) {
			log.debug("Database flag cannot be retrieve");
			df = new DatabaseFlag();
		}
		df.setBuilt(true);
		ds.save(df);
		return RestUtils.createResponse(true);
	}
	
	@GET
	@Path("zone/all")
	@Produces(Utils.SERVICE_PRODUCES)
	public Response getAllProviders() {
		Datastore ds = Utils.getMongoConnection().getDatastore();
		List<ProviderInfo> result = ds.find(ProviderInfo.class).asList();
		return RestUtils.createResponse(result);
	}
}
