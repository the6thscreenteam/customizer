package fr.the6thscreen.yourcast.customizer.authentication;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.apache.log4j.Logger;

import fr.the6thscreen.yourcast.customizer.service.CrudService;
import fr.yourcast.sources.utils.SourceExceptionHandler;

public class SessionManager {

	private Map<String,AuthenticationToken> authorizedAccess;
	private static SessionManager instance;
	private static Logger log = Logger.getLogger(SessionManager.class);
	
	private SessionManager() {
		this.authorizedAccess = new HashMap<String,AuthenticationToken>();
	}
	
	public static SessionManager getInstance() {
		if (instance == null) {
			instance = new SessionManager();
		}
		return instance;
	}
	
	private boolean canBeAuthorized(String token) {
		if (this.authorizedAccess.containsKey(token)) {
			AuthenticationToken at = this.authorizedAccess.get(token);
			if (at.isTokenExpired()) {
				this.authorizedAccess.remove(token);
				log.debug("Token "+token+" has expired.");
				return false;
			}
			at.updateExpirationDate();
			return true;
		}
		log.debug("Token "+token+" not found.");
		return false;
	}
	
	public void testAuthorizationAndThrowError(String token) {
		if (token == null || !this.canBeAuthorized(token)) {
			throw SourceExceptionHandler.createException(new IllegalAccessException("Unauthorized access"), 405);
		}
	}
	
	public String createAuthenticationToken() {
		AuthenticationToken at = new AuthenticationToken();
		UUID token = UUID.randomUUID();
		this.authorizedAccess.put(token.toString(), at);
		return token.toString();
	}
}
