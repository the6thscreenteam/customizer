package fr.the6thscreen.yourcast.customizer.service;

import javax.ws.rs.core.Response;

public class RestUtils {
	
	private static final String HEADER_CORS_VALUE = "*";
	
	public static Response createResponse(Object result) {
		return Response.ok(result)
				.header("Access-Control-Allow-Origin",HEADER_CORS_VALUE)
				.header("Access-Control-Allow-Credentials", "true")
				.header("Access-Control-Allow-Methods","OPTIONS, GET, POST, PUT, DELETE")
				.header("Access-Control-Allow-Headers","Content-Type, Depth, User-Agent, X-File-Size, X-Requested-With, If-Modified-Since, X-File-Name, Cache-Control")
				.build();
	}
}
