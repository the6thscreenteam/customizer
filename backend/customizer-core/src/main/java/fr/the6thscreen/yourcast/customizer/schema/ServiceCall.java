package fr.the6thscreen.yourcast.customizer.schema;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.log4j.Logger;

import com.google.code.morphia.annotations.Embedded;
import com.google.code.morphia.annotations.Entity;
import com.google.code.morphia.annotations.Id;
import com.google.code.morphia.annotations.Reference;

import fr.the6thscreen.yourcast.customizer.service.ServiceCallService;

@XmlRootElement
@Entity
public class ServiceCall {
	private static Logger log = Logger.getLogger(ServiceCall.class);
	@Id private String callID;
	@Reference private List<Calls> parameters;
	@Reference private ProviderInfo provider;
	@Reference private ServiceInfo service;
	
	public ServiceCall() {
		this.parameters = new ArrayList<Calls>();
	}
	
	public ServiceCall(String callID, ProviderInfo provider, ServiceInfo service) {
		this();
		this.callID = callID;
		this.provider = provider;
		this.service = service;
	}
	
	@XmlElement
	public String getCallID() {
		return callID;
	}
	
	public void setCallID(String callID) {
		this.callID = callID;
	}
	
	@XmlElement
	public List<Calls> getParameters() {
		return parameters;
	}
	
	public void setParameters(List<Calls> parameters) {
		this.parameters = parameters;
	}
	
	@XmlElement
	public ProviderInfo getProvider() {
		return provider;
	}
	
	public void setProvider(ProviderInfo provider) {
		this.provider = provider;
	}
	
	@XmlElement
	public ServiceInfo getService() {
		return service;
	}
	
	public void setService(ServiceInfo service) {
		this.service = service;
	}
	
	public void removeParameter(String id) {
		Calls todelete = null;
		for (Calls c : this.parameters) {
			if (c.getId().equals(id)) {
				todelete = c;
			}
		}
		if (todelete != null) {
			log.debug("Delete parameter id : "+id+" !");
			this.parameters.remove(todelete);
		} else {
			log.debug("Parameter id "+id+" not found to be deleted...");
		}
	}
}
