package fr.the6thscreen.yourcast.customizer.schema;

import java.util.UUID;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


import com.google.code.morphia.annotations.Entity;
import com.google.code.morphia.annotations.Id;

@XmlRootElement
@Entity
public class RateLimit {

	@Id private String id;
	private int minutes;
	private int requests;
	
	public RateLimit(int m, int r) {
		this.id = UUID.randomUUID().toString();
		this.minutes = m;
		this.requests = r;
	}
	
	public RateLimit() {
		this(-1,-1);
	}
	
	@XmlElement
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@XmlElement
	public int getMinutes() {
		return minutes;
	}

	public void setMinutes(int minutes) {
		this.minutes = minutes;
	}

	@XmlElement
	public int getRequests() {
		return requests;
	}

	public void setRequests(int requests) {
		this.requests = requests;
	}
	
	
}
