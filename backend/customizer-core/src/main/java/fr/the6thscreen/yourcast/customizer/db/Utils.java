package fr.the6thscreen.yourcast.customizer.db;

import javax.ws.rs.core.MediaType;

public class Utils {
	private static MongoConnection instance;
	public static final String SERVICE_PRODUCES = MediaType.APPLICATION_JSON+";charset=UTF-8";
	
	public static MongoConnection getMongoConnection() {
		if (instance == null) {
			try {
				Class<MongoConnection> mcClass = (Class<MongoConnection>) Class.forName("fr.yourcast.personalizer.db.DBConnection");
				instance = mcClass.newInstance();
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InstantiationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return instance;
	}
}
