package fr.the6thscreen.yourcast.customizer.authentication;

import java.util.Calendar;

public class AuthenticationToken {

	private Calendar expirationDate;
	
	protected AuthenticationToken() {
		this.expirationDate = Calendar.getInstance();
		this.updateExpirationDate();
	}
	
	protected void updateExpirationDate() {
		this.expirationDate.add(Calendar.HOUR, 1);
	}
	
	protected boolean isTokenExpired() {
		return Calendar.getInstance().after(expirationDate);
	}
}
