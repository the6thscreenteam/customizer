package fr.the6thscreen.yourcast.customizer.service;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import com.google.code.morphia.Datastore;

import fr.the6thscreen.yourcast.customizer.authentication.SessionManager;
import fr.the6thscreen.yourcast.customizer.db.Utils;
import fr.the6thscreen.yourcast.customizer.schema.AuthorizedUser;
import fr.yourcast.sources.utils.SourceExceptionHandler;

@Path("login/")
public class AuthenticationService {

	@POST
	@Path("{login}")
	@Produces(Utils.SERVICE_PRODUCES)
	@Consumes(Utils.SERVICE_PRODUCES)
	public Response authenticate(@PathParam("login") String login, String pwd) {
		Datastore ds = Utils.getMongoConnection().getDatastore();
		AuthorizedUser au = ds.find(AuthorizedUser.class, "login =", login).filter("password = ", pwd).get();
		if (au != null) {
			String token = SessionManager.getInstance().createAuthenticationToken();
			return RestUtils.createResponse(token);
		} else {
			throw SourceExceptionHandler.createException(new IllegalAccessException("Unauthorized access"), 405);
		}
	}
}
