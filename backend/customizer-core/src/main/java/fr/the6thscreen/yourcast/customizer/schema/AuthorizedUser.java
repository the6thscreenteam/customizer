package fr.the6thscreen.yourcast.customizer.schema;

import com.google.code.morphia.annotations.Entity;
import com.google.code.morphia.annotations.Id;

@Entity
public class AuthorizedUser {

	@Id private String login;
	private String password;
	
	public AuthorizedUser() {}
	
	public AuthorizedUser(String login, String pwd) {
		this.login = login;
		this.password = pwd;
	}
	
	public String getLogin() {
		return login;
	}
	
	public void setLogin(String login) {
		this.login = login;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	
}
