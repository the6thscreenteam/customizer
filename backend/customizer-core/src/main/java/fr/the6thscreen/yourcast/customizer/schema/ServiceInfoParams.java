package fr.the6thscreen.yourcast.customizer.schema;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.google.code.morphia.annotations.Entity;
import com.google.code.morphia.annotations.Id;

@XmlRootElement
@Entity
public class ServiceInfoParams {

	@Id private String id;
	private String uri;
	private Map<String, String> query;
	private Map<String, String> headers;
	private String body;
	private String path;
	
	public ServiceInfoParams() {
		this.id = UUID.randomUUID().toString();
		this.query = new HashMap<String,String>();
		this.headers = new HashMap<String,String>();
	}
	
	public ServiceInfoParams(String uri, String path) {
		this();
		this.uri = uri;
		this.path = path;
	}
	
	@XmlElement
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@XmlElement
	public String getUri() {
		return uri;
	}
	
	public void setUri(String uri) {
		this.uri = uri;
	}
	
	@XmlElement
	public Map<String, String> getQuery() {
		return query;
	}
	
	public void setQuery(Map<String, String> query) {
		this.query = query;
	}
	
	@XmlElement
	public Map<String, String> getHeaders() {
		return headers;
	}
	
	public void setHeaders(Map<String, String> header) {
		this.headers = header;
	}
	
	@XmlElement
	public String getBody() {
		return body;
	}
	
	public void setBody(String body) {
		this.body = body;
	}
	
	@XmlElement
	public String getPath() {
		return path;
	}
	
	public void setPath(String path) {
		this.path = path;
	}
	
	
}
