package fr.the6thscreen.yourcast.customizer.schema;

import com.google.code.morphia.annotations.Entity;
import com.google.code.morphia.annotations.Id;

@Entity
public class DatabaseFlag {

	@Id private String id;
	private boolean isBuilt;
	
	public DatabaseFlag() {
		this.isBuilt = false;
	}

	public boolean isBuilt() {
		return isBuilt;
	}

	public void setBuilt(boolean isBuilt) {
		this.isBuilt = isBuilt;
	}
}
