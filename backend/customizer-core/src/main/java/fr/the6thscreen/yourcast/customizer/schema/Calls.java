package fr.the6thscreen.yourcast.customizer.schema;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.google.code.morphia.annotations.Entity;
import com.google.code.morphia.annotations.Id;

@XmlRootElement
@Entity
public class Calls {

	@Id private String id;
	private String method;
	private Map<String,String> query;
	private Map<String,String> headers;
	private String path;
	private String body;
	
	public Calls() {
		this.id = UUID.randomUUID().toString();
		this.query = new HashMap<String,String>();
		this.headers = new HashMap<String,String>();
	}
	
	@XmlElement
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@XmlElement
	public String getMethod() {
		return method;
	}
	
	public void setMethod(String method) {
		this.method = method;
	}
	
	@XmlElement
	public Map<String, String> getQuery() {
		return query;
	}
	
	public void setQuery(Map<String, String> query) {
		this.query = query;
	}
	
	@XmlElement
	public Map<String, String> getHeaders() {
		return headers;
	}
	
	public void setHeaders(Map<String, String> headers) {
		this.headers = headers;
	}
	
	@XmlElement
	public String getPath() {
		return path;
	}
	
	public void setPath(String path) {
		this.path = path;
	}
	
	@XmlElement
	public String getBody() {
		return body;
	}
	
	public void setBody(String body) {
		this.body = body;
	}
	
}
