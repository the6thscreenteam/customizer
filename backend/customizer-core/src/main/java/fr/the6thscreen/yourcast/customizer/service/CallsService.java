package fr.the6thscreen.yourcast.customizer.service;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.apache.log4j.Logger;

import com.google.code.morphia.Datastore;
import com.google.code.morphia.Key;
import com.google.code.morphia.query.UpdateOperations;

import fr.the6thscreen.yourcast.customizer.authentication.SessionManager;
import fr.the6thscreen.yourcast.customizer.db.Utils;
import fr.the6thscreen.yourcast.customizer.schema.Calls;
import fr.the6thscreen.yourcast.customizer.schema.ProviderFlag;
import fr.the6thscreen.yourcast.customizer.schema.ProviderInfo;
import fr.the6thscreen.yourcast.customizer.schema.ServiceCall;

@Path("call/")
public class CallsService {
	private static Logger log = Logger.getLogger(CallsService.class);
	@GET
	@Produces(Utils.SERVICE_PRODUCES)
	public Response getAllCalls() {
		Datastore ds = Utils.getMongoConnection().getDatastore();
		List<Calls> result = ds.find(Calls.class).asList();
		return RestUtils.createResponse(result);
	}
	
	@GET
	@Path("{id}")
	@Produces(Utils.SERVICE_PRODUCES)
	public Response getCalls(@PathParam("id") String id) {
		Datastore ds = Utils.getMongoConnection().getDatastore();
		Calls result = ds.get(Calls.class, id);
		return RestUtils.createResponse(result);
	}
	
	@GET
	@Path("callID/{callID}")
	@Produces(Utils.SERVICE_PRODUCES)
	public Response getCallsFromCallID(@PathParam("callID") String id) {
		Datastore ds = Utils.getMongoConnection().getDatastore();
		ServiceCall sc = ds.get(ServiceCall.class, id);
		List<Calls> result = sc.getParameters();
		return RestUtils.createResponse(result);
	}
	
	@POST
	@Path("edit/{callID}/{id}")
	@Produces(Utils.SERVICE_PRODUCES)
	@Consumes(Utils.SERVICE_PRODUCES)
	public Response editCalls(@QueryParam("token") String token, @PathParam("callID") String callID, @PathParam("id") String id, Calls call) {
		SessionManager.getInstance().testAuthorizationAndThrowError(token);
		Datastore ds = Utils.getMongoConnection().getDatastore();
		ServiceCall sc = ds.get(ServiceCall.class, callID);
		if (sc == null) {
			return RestUtils.createResponse(false);
		}
		ds.merge(call);
		this.updateProviderFlag(sc, ds);
		return RestUtils.createResponse(true);
	}
	
	@Path("delete/{callID}/{id}")
	@OPTIONS
	@Deprecated
	public Response optionsForDeleteCalls(@HeaderParam("Access-Control-Request-Headers") String requestH) {
		ResponseBuilder rb = Response.ok().header("Access-Control-Allow-Origin", "*").header("Access-Control-Allow-Methods", "GET, POST, PUT, OPTIONS, DELETE");
		rb.header("Access-Control-Allow-Headers", requestH);
		return rb.build();
	}
	
	@DELETE
	@Path("delete/{callID}/{id}")
	@Produces(Utils.SERVICE_PRODUCES)
	public Response deleteCalls(@QueryParam("token") String token, @PathParam("callID") String callID, @PathParam("id") String id) {
		SessionManager.getInstance().testAuthorizationAndThrowError(token);
		Datastore ds = Utils.getMongoConnection().getDatastore();
		
		ServiceCall sc = ds.get(ServiceCall.class, callID);
		if (sc == null) {
			return RestUtils.createResponse(false);
		}
		sc.removeParameter(id);
		log.debug("Paramter removed. New list of parameters : "+sc.getParameters());
		if (sc.getParameters().isEmpty()) {
			UpdateOperations<ServiceCall> ops =  ds.createUpdateOperations(ServiceCall.class).set("parameters", sc.getParameters());
			ds.update(sc, ops);
		} else {
			ds.merge(sc);
		}
		ds.delete(Calls.class, id);
		
		this.updateProviderFlag(sc, ds);
		return RestUtils.createResponse(true);
	}
	
	private void updateProviderFlag(ServiceCall sc, Datastore ds) {
		ProviderInfo pi = sc.getProvider();
		ProviderFlag pf = ds.find(ProviderFlag.class, "provider = ", pi.getName()).get();
		if (pf != null) {
			pf.setFlag(sc.getService().getName(), true);
			ds.merge(pf);
		} else {
			pf = new ProviderFlag(pi);
			pf.setFlag(sc.getService().getName(), true);
			ds.save(pf);
		}
	}
	
	@POST
	@Path("add/{callID}")
	@Produces(Utils.SERVICE_PRODUCES)
	@Consumes(Utils.SERVICE_PRODUCES)
	public Response addCalls(@QueryParam("token") String token, @PathParam("callID") String id, Calls call) {
		SessionManager.getInstance().testAuthorizationAndThrowError(token);
		Datastore ds = Utils.getMongoConnection().getDatastore();
		ds.save(call);
		ServiceCall sc = ds.get(ServiceCall.class, id);
		
		if (sc == null) {
			return RestUtils.createResponse(false);
		}
		if (sc.getParameters() == null) {
			List<Calls> lcalls = new ArrayList<Calls>();
			lcalls.add(call);
			sc.setParameters(lcalls);
		} else {
			sc.getParameters().add(call);
		}
		ds.merge(sc);
		
		this.updateProviderFlag(sc, ds);
		
		return RestUtils.createResponse(true);
	}
}
