package fr.the6thscreen.yourcast.customizer.schema;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.google.code.morphia.annotations.Embedded;
import com.google.code.morphia.annotations.Entity;
import com.google.code.morphia.annotations.Id;

@XmlRootElement
@Entity
public class ServiceInfo {

	@Embedded private ServiceInfoParams args;
	@Id private String name;
	@Embedded private RateLimit rateLimit;
	
	public ServiceInfo(String name, ServiceInfoParams sip, RateLimit rateLimit) {
		this.name = name;
		this.args = sip;
		this.rateLimit = rateLimit;
	}
	
	public ServiceInfo(String name, ServiceInfoParams sip) {
		this(name,sip, new RateLimit());
	}
	
	public ServiceInfo() {
		this("",new ServiceInfoParams(), new RateLimit());
	}
	
	@XmlElement
	public ServiceInfoParams getArgs() {
		return args;
	}

	public void setArgs(ServiceInfoParams args) {
		this.args = args;
	}

	@XmlElement
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@XmlElement
	public RateLimit getRateLimit() {
		return rateLimit;
	}

	public void setRateLimit(RateLimit rateLimit) {
		this.rateLimit = rateLimit;
	}
	
	
}
