package fr.the6thscreen.yourcast.customizer.schema;

import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.google.code.morphia.annotations.Entity;
import com.google.code.morphia.annotations.Id;

@XmlRootElement
@Entity
public class ProviderFlag {

	private Map<String, Boolean> flags;
	@Id private String provider; 
	
	public ProviderFlag() {
		this.flags = new HashMap<String, Boolean>();
	}
	
	public ProviderFlag(ProviderInfo pi) {
		this();
		this.provider = pi.getName();
	}

	@XmlElement
	public Map<String, Boolean> getFlags() {
		return flags;
	}

	public void setFlags(Map<String, Boolean> flags) {
		this.flags = flags;
	}

	@XmlElement
	public String getProvider() {
		return provider;
	}

	public void setProvider(String provider) {
		this.provider = provider;
	}
	
	public boolean getFlag(String s) {
		if (this.flags.containsKey(s)) {
			return this.flags.get(s);
		}
		return false;
	}
	
	public void setFlag(String s, boolean v) {
		this.flags.put(s, v);
	}
}
