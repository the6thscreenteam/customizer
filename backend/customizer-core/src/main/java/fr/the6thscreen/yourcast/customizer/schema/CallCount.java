package fr.the6thscreen.yourcast.customizer.schema;

import java.util.UUID;

import com.google.code.morphia.annotations.Entity;
import com.google.code.morphia.annotations.Id;
import com.google.code.morphia.annotations.Reference;

@Entity
public class CallCount {

	@Id private String id;
	private int callExecuted;
	@Reference private ProviderInfo provider;
	@Reference private ServiceInfo source;
	
	public CallCount() {
		this.id = UUID.randomUUID().toString();
	}
	
	public CallCount(ProviderInfo pi, ServiceInfo si) {
		this();
		this.provider = pi;
		this.source = si;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getCallExecuted() {
		return callExecuted;
	}

	public void setCallExecuted(int callExecuted) {
		this.callExecuted = callExecuted;
	}

	public ProviderInfo getProvider() {
		return provider;
	}

	public void setProvider(ProviderInfo provider) {
		this.provider = provider;
	}

	public ServiceInfo getSource() {
		return source;
	}

	public void setSource(ServiceInfo source) {
		this.source = source;
	}
}
