package fr.the6thscreen.yourcast.customizer.schema;

import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.google.code.morphia.annotations.Entity;
import com.google.code.morphia.annotations.Id;

@XmlRootElement
@Entity
public class ClientCustomization {

	@Id private String provider;
	private Map<String,Integer> mapTime;
	private Map<String,Integer> mapOrder;
	private String requestUrl;
	private Integer requestTimeout;
	
	public ClientCustomization(ProviderInfo provider) {
		this.provider = provider.getName();
		this.mapTime = new HashMap<String,Integer>();
		this.mapOrder = new HashMap<String,Integer>();
	}
	
	public ClientCustomization(ProviderInfo provider, Map<String,Integer> mapTime, Map<String,Integer> mapOrder, String requestUrl, Integer requestTimeout) {
		this.provider = provider.getName();
		this.mapOrder = mapOrder;
		this.mapTime = mapTime;
		this.requestTimeout = requestTimeout;
		this.requestUrl = requestUrl;
	}
	
	public ClientCustomization() {
		this(new ProviderInfo());
	}
	
	@XmlElement
	public String getProvider() {
		return this.provider;
	}
	
	public void setProvider(String provider) {
		this.provider = provider;
	}
	
	@XmlElement
	public Map<String, Integer> getMapTime() {
		return mapTime;
	}
	
	public void setMapTime(Map<String, Integer> mapTime) {
		this.mapTime = mapTime;
	}
	
	@XmlElement
	public Map<String, Integer> getMapOrder() {
		return mapOrder;
	}
	
	public void setMapOrder(Map<String, Integer> mapOrder) {
		this.mapOrder = mapOrder;
	}
	
	@XmlElement
	public String getRequestUrl() {
		return requestUrl;
	}
	
	public void setRequestUrl(String requestUrl) {
		this.requestUrl = requestUrl;
	}
	
	@XmlElement
	public Integer getRequestTimeout() {
		return requestTimeout;
	}
	
	public void setRequestTimeout(Integer requestTimeout) {
		this.requestTimeout = requestTimeout;
	}
	
	
}
