package fr.the6thscreen.yourcast.customizer.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import com.google.code.morphia.Datastore;

import fr.the6thscreen.yourcast.customizer.db.Utils;
import fr.the6thscreen.yourcast.customizer.schema.CallCount;
import fr.the6thscreen.yourcast.customizer.schema.Calls;
import fr.the6thscreen.yourcast.customizer.schema.ProviderFlag;
import fr.the6thscreen.yourcast.customizer.schema.ProviderInfo;
import fr.the6thscreen.yourcast.customizer.schema.RateLimit;
import fr.the6thscreen.yourcast.customizer.schema.ServiceCall;
import fr.the6thscreen.yourcast.customizer.schema.ServiceInfo;
import fr.the6thscreen.yourcast.customizer.schema.ServiceInfoParams;

@Path("provider/")
public class ProviderService {

	@GET
	@Path("params/{sourceName}")
	@Produces(Utils.SERVICE_PRODUCES)
	public ServiceInfoParams getParamSource(@PathParam("sourceName") String sourceName) {
		Datastore ds = Utils.getMongoConnection().getDatastore();
		ServiceInfo si = ds.find(ServiceInfo.class, "name = ", sourceName).get();
		return si.getArgs();
	}
	
	@GET
	@Path("calls/{zoneName}/{sourceName}")
	@Produces(Utils.SERVICE_PRODUCES)
	public Map<String,List<Calls>> getParamSource(@PathParam("sourceName") String sourceName,@PathParam("zoneName") String zoneName) {
		Datastore ds = Utils.getMongoConnection().getDatastore();
		ServiceInfo si = ds.find(ServiceInfo.class, "name = ", sourceName).get();
		ProviderInfo pi = ds.find(ProviderInfo.class, "name = ", zoneName).get();
		List<ServiceCall> lsc = ds.find(ServiceCall.class, "service =", si).filter("provider = ", pi).asList();
		Map<String,List<Calls>> result = new HashMap<String,List<Calls>>();
		for (ServiceCall sc : lsc) {
			result.put(sc.getCallID(), sc.getParameters());
		}
		return result;
	}
	
	@GET
	@Path("flags/{zoneName}/{sourceName}")
	@Produces(Utils.SERVICE_PRODUCES)
	public Map<String,Boolean> getFlagOfZone(@PathParam("sourceName") String sourceName, @PathParam("zoneName") String zoneName) {
		Datastore ds = Utils.getMongoConnection().getDatastore();
		ProviderInfo pi = ds.find(ProviderInfo.class, "name = ", zoneName).get();
		ProviderFlag pf = ds.find(ProviderFlag.class, "provider = ", pi.getName()).get();
		if (pf != null) {
			Boolean value = pf.getFlag(sourceName);
			Map<String,Boolean> result = new HashMap<String,Boolean>();
			result.put("value",value);
			return result;
		} else {
			return null;
		}
	}
	
	@POST
	@Path("flags/update/{zoneName}/{sourceName}/{value}")
	@Produces(Utils.SERVICE_PRODUCES)
	public Map<String,Boolean> updateFlagOfZone(@PathParam("sourceName") String sourceName, @PathParam("zoneName") String zoneName, @PathParam("value") boolean value) {
		Datastore ds = Utils.getMongoConnection().getDatastore();
		ProviderInfo pi = ds.find(ProviderInfo.class, "name = ", zoneName).get();
		ProviderFlag pf = ds.find(ProviderFlag.class, "provider = ", pi.getName()).get();
		if (pf != null) {
			pf.setFlag(sourceName, value);
			ds.merge(pf);
		} else {
			pf = new ProviderFlag(pi);
			pf.setFlag(sourceName, value);
			ds.save(pf);
		}
		Map<String,Boolean> result = new HashMap<String,Boolean>();
		result.put("result",true);
		return result;
	}
	
	@GET
	@Path("rateLimit/{sourceName}")
	@Produces(Utils.SERVICE_PRODUCES)
	public RateLimit getRateLimit(@PathParam("sourceName") String sourceName) {
		Datastore ds = Utils.getMongoConnection().getDatastore();
		ServiceInfo si = ds.find(ServiceInfo.class, "name = ", sourceName).get();
		return si.getRateLimit();
	}
	
	@GET
	@Path("callCount/{zoneName}/{sourceName}")
	@Produces(Utils.SERVICE_PRODUCES)
	public Map<String,Integer> getCallCount(@PathParam("sourceName") String sourceName, @PathParam("zoneName") String zoneName) {
		Datastore ds = Utils.getMongoConnection().getDatastore();
		ProviderInfo pi = ds.find(ProviderInfo.class, "name = ", zoneName).get();
		ServiceInfo si = ds.find(ServiceInfo.class, "name = ", sourceName).get();
		CallCount cc = ds.find(CallCount.class, "provider = ", pi).filter("source = ", si).get();
		if (cc != null) {
			Map<String,Integer> result = new HashMap<String,Integer>();
			result.put("value",cc.getCallExecuted());
			return result;
		} else {
			return null;
		}
	}
	
	@POST
	@Path("callCount/update/{zoneName}/{sourceName}/{value}")
	@Produces(Utils.SERVICE_PRODUCES)
	public Map<String,Boolean> updateCallCount(@PathParam("sourceName") String sourceName, @PathParam("zoneName") String zoneName, @PathParam("value") int value) {
		Datastore ds = Utils.getMongoConnection().getDatastore();
		ProviderInfo pi = ds.find(ProviderInfo.class, "name = ", zoneName).get();
		ServiceInfo si = ds.find(ServiceInfo.class, "name = ", sourceName).get();
		CallCount cc = ds.find(CallCount.class, "provider = ", pi).filter("source = ", si).get();
		if (cc != null) {
			cc.setCallExecuted(value);
			ds.merge(cc);
		} else {
			cc = new CallCount(pi,si);
			cc.setCallExecuted(value);
			ds.save(cc);
		}
		Map<String,Boolean> result = new HashMap<String,Boolean>();
		result.put("result",true);
		return result;
	}
}
