package fr.the6thscreen.yourcast.customizer.schema;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.google.code.morphia.annotations.Entity;
import com.google.code.morphia.annotations.Id;

@XmlRootElement
@Entity
public class ProviderInfo {
	
	 @Id private String name;
	
	public ProviderInfo() {}
	
	public ProviderInfo(String name) {
		this();
		this.name = name;
	}
	
	@XmlElement
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
}
