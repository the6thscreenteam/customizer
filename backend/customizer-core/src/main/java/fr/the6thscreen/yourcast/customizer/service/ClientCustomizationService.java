package fr.the6thscreen.yourcast.customizer.service;

import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import com.google.code.morphia.Datastore;

import fr.the6thscreen.yourcast.customizer.authentication.SessionManager;
import fr.the6thscreen.yourcast.customizer.db.Utils;
import fr.the6thscreen.yourcast.customizer.schema.ClientCustomization;
import fr.the6thscreen.yourcast.customizer.schema.ProviderInfo;

@Path("client/")
public class ClientCustomizationService {

	@GET
	@Path("{providerName}")
	@Produces(Utils.SERVICE_PRODUCES)
	public Response getClientInfo(@PathParam("providerName") String providerName) {
		Datastore ds = Utils.getMongoConnection().getDatastore();
		ClientCustomization result = ds.get(ClientCustomization.class, providerName);
		return RestUtils.createResponse(result);
	}
	
	@PUT
	@Path("{providerName}/{callID}/{time}")
	@Produces(Utils.SERVICE_PRODUCES)
	public Response changeCallIDTime(@QueryParam("token") String token, @PathParam("providerName") String providerName, @PathParam("callID") String callID, @PathParam("time") int time) {
		SessionManager.getInstance().testAuthorizationAndThrowError(token);
		Datastore ds = Utils.getMongoConnection().getDatastore();
		ProviderInfo pi = ds.find(ProviderInfo.class, "name = ", providerName).get();
		if (pi != null) {
			ClientCustomization custom = ds.get(ClientCustomization.class, pi.getName());
			if (custom == null) {
				custom = new ClientCustomization(pi);
			}
			Map<String,Integer> mapTime = custom.getMapTime();
			mapTime.put(callID, time);
			ds.merge(custom);
			return RestUtils.createResponse(true);
		}
		return RestUtils.createResponse(false);
	}
	
	@POST
	@Path("{providerName}/{requestTimeout}")
	@Produces(Utils.SERVICE_PRODUCES)
	public Response changeRequestTimeout(@QueryParam("token") String token, @PathParam("providerName") String providerName, @PathParam("requestTimeout") int timeout, String requestUrl) {
		SessionManager.getInstance().testAuthorizationAndThrowError(token);
		Datastore ds = Utils.getMongoConnection().getDatastore();
		ProviderInfo pi = ds.find(ProviderInfo.class, "name = ", providerName).get();
		if (pi != null) {
			ClientCustomization custom = ds.get(ClientCustomization.class, pi.getName());
			if (custom == null) {
				custom = new ClientCustomization(pi);
			}
			custom.setRequestTimeout(timeout);
			custom.setRequestUrl(requestUrl);
			ds.merge(custom);
			return RestUtils.createResponse(true);
		}
		return RestUtils.createResponse(false);
	}
	
	@POST
	@Path("{providerName}/mapOrder")
	@Produces(Utils.SERVICE_PRODUCES)
	@Consumes(Utils.SERVICE_PRODUCES)
	public Response setMapOrder(@QueryParam("token") String token, @PathParam("providerName") String providerName, Map<String,Integer> mapOrder) {
		SessionManager.getInstance().testAuthorizationAndThrowError(token);
		Datastore ds = Utils.getMongoConnection().getDatastore();
		ProviderInfo pi = ds.find(ProviderInfo.class, "name = ", providerName).get();
		if (pi != null) {
			ClientCustomization custom = ds.get(ClientCustomization.class, pi.getName());
			if (custom == null) {
				custom = new ClientCustomization(pi);
			}
			custom.setMapOrder(mapOrder);
			ds.merge(custom);
			return RestUtils.createResponse(true);
		}
		return RestUtils.createResponse(false);
	}
}
