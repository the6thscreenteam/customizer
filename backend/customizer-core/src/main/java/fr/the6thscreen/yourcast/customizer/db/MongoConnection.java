package fr.the6thscreen.yourcast.customizer.db;

import com.google.code.morphia.Datastore;
import com.google.code.morphia.Morphia;

import fr.the6thscreen.yourcast.customizer.schema.*;
import fr.yourcast.libraries.database.MongoDatabase;

public abstract class MongoConnection {
	private static MongoDatabase mongoDB = MongoDatabase.getInstance();
	
	public Datastore getDatastore() {
		Morphia m = mongoDB.getMorphia();
		m.map(ProviderInfo.class);
		m.map(ServiceInfo.class);
		m.map(ServiceCall.class);
		m.map(Calls.class);
		m.map(ProviderFlag.class);
		m.map(RateLimit.class);
		m.map(ServiceInfoParams.class);
		m.map(CallCount.class);
		m.map(ClientCustomization.class);
		m.map(DatabaseFlag.class);
		m.map(AuthorizedUser.class);
		
		return mongoDB.createOrGetDatabase(getDBName());
	}
	
	public boolean isDatabaseExisting() {
		return mongoDB.databaseExists(this.getDBName());
	}
	
	public abstract void createDatabase();
	protected abstract String getDBName();
}
