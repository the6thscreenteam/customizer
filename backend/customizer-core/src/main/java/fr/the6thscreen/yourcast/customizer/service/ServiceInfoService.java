package fr.the6thscreen.yourcast.customizer.service;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import com.google.code.morphia.Datastore;

import fr.the6thscreen.yourcast.customizer.db.Utils;
import fr.the6thscreen.yourcast.customizer.schema.ServiceInfo;

@Path("si/")
public class ServiceInfoService {
	@GET
	@Produces(Utils.SERVICE_PRODUCES)
	public Response getAllServiceInfos() {
		Datastore ds = Utils.getMongoConnection().getDatastore();
		List<ServiceInfo> result = ds.find(ServiceInfo.class).asList();
		return RestUtils.createResponse(result);
	}
	
	@GET
	@Path("{id}")
	@Produces(Utils.SERVICE_PRODUCES)
	public Response getServiceInfo(@PathParam("id") String id) {
		Datastore ds = Utils.getMongoConnection().getDatastore();
		ServiceInfo result = ds.find(ServiceInfo.class, "id =", id).get();
		return RestUtils.createResponse(result);
	}
}
